# Package

version       = "0.0.1"
author        = "joachimschmidt557"
description   = "Mandelbrot in nim"
license       = "GPL-3.0"
bin           = @["main"]


# Dependencies

requires "nim >= 0.19.9"
requires "stbimage >= 2.3"
requires "nimbmp >= 0.1.6"
