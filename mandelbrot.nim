import sequtils, math, complex, colors, hsb

proc getComplexValueForPixel(x:int, y:int, height:int, width:int, zoom:float64): Complex[float64] =
    complex64((x.float64-width.float64/2.0'f64)/zoom, (y.float64-height.float64/2.0'f64)/zoom)

proc colorMap(i:int, nMax:int): Color =
    let
       h = if (float(i) / 100'f64) <= 1.0:
              (float(i) / 100'f64) * 360'f64
           else: 360'f64
       s = 1'f64
       v = if i < nMax: 1'f64 else: 0'f64

    result = hsbToRgb(h, s, v)

proc generatePixel(x:int, y:int, height:int, width:int, zoom:float64,
                   rMax: int, maxIter:int) : Color =
   let c = getComplexValueForPixel(x, y, height, width, zoom)

   var
      n = maxIter
      z = complex64(0'f64, 0'f64)

   for i in 0..maxIter-1:
      if z.abs >= rMax.float64:
         n = i
         break
      z = c + z * z
   
   result = colorMap(n, maxIter)

proc generateImage*(width:int, height:int, zoom:float64, rMax:int, maxIter:int): seq[seq[Color]] =

   result = newSeqWith(height, newSeq[Color](width))

   for y in 0..height-1:
      for x in 0..width-1:
         result[y][x] = generatePixel(x, y, height, width, zoom, rMax, maxIter)
