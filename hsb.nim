import math, colors

proc hsbToRgb*(h:float64, s:float64, v:float64): Color =
   var
      H = h
      R = 0'f64
      G = 0'f64
      B = 0'f64

   while H < 0'f64:
      H = H + 360'f64
   while H >= 360'f64:
      H = H - 360'f64

   if v <= 0'f64:
      R = 0'f64
      G = 0'f64
      B = 0'f64
   elif s <= 0'f64:
      R = v
      G = v
      B = v
   else:
      let
         hf = H / 60'f64
         i = int(floor(hf))
         f = hf - float64(i)
         pv = v * (1'f64 - s)
         qv = v * (1'f64 - s * f)
         tv = v * (1'f64 - (1'f64 - f))

      case i:
         of 0:
            R = v; G = tv; B = pv
         of 1: R = qv; G = v; B = pv
         of 2: R = pv; G = v; B = tv
         of 3: R = pv; G = qv; B = v
         of 4: R = tv; G = pv; B = v
         of 5: R = v; G = pv; B = qv
         of 6: R = v; G = tv; B = pv
         of -1: R = v; G = pv; B = qv
         else:
            R = v; G = v; B = v

   result = rgb(clamp(int(R*255), 0, 255),
                clamp(int(G*255), 0, 255),
                clamp(int(B*255), 0, 255))
