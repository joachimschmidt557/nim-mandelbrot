import mandelbrot, colors, nimBMP

let img = mandelbrot.generateImage(800, 600, 150'f64, 32, 570)
var raw:seq[uint8]

for y in img:
    for x in y:
        let rgb = x.extractRGB()
        raw.add(rgb.r.uint8)
        raw.add(rgb.g.uint8)
        raw.add(rgb.b.uint8)

saveBMP24("mandelbrot.bmp", raw, 800, 600)

